import React from 'react';
import Styles from './Cockpit.css'

const cockpit = (props) => {

    const classes = [];
    let btnClass = '';

    if(props.showlist){
        btnClass = Styles.Red;
    }   

    if(props.persons.length <=3 ){
      classes.push(Styles.red)
    }
    if(props.persons.length <=2 ){
      classes.push(Styles.bold)
    }

    return (
        <div className={Styles.Cockpit}>
            <p className={classes.join(' ')}>Welcome to React Program </p>       
            <p><button className={btnClass} 
                onClick={props.toggleList}>Toggle List</button></p> 
        </div>
    );
}
 
export default cockpit;