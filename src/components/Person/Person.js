import React from 'react';

import Styles from './Person.css';

const person = ( props ) => {
    //console.log(props)
    // const style = {
    //     '@media (min-width: 400px)' : {
    //         width: '350px'
    //     }
    //}
    return (
        
        <div className={Styles.Person}>
            <p onClick={props.click} >I'm {props.fname} and I am {props.lname} years old!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.fname} />
        </div>
    )
};

export default person;