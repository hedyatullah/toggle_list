import React, { Component } from 'react';
//import Radium, { StyleRoot } from 'radium'
import Styles from './App.css';
import Persons from '../components/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {
  state = {
    persons: [
      {id: 1, firstname:'Hedyat', age:40},
      {id: 2, firstname:'Fouzia', age:34},
      {id: 3, firstname:'Aaliyah', age:7},
      {id: 4, firstname:'Rayyan', age:4},
      {id: 5, firstname:'Asma', age: 'less than a'}    
    ],
    showlist: false
  }
  toggleList = () => {    
    const showperson = this.state.showlist;
    this.setState({showlist: !showperson})
  }
  deletePerson = (index) => {
    const persons = [...this.state.persons];
    persons.splice(index,1);
    this.setState({persons: persons})
  }
  changePersonName = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    })
    const person = {...this.state.persons[personIndex]}
    person.firstname = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({persons: persons})
  }
  render(){  
    let persons = null;    
    if(this.state.showlist === true){
      persons = <Persons 
            persons={this.state.persons}
            clicked={this.deletePerson}
            changed={this.changePersonName}
          />                      
    }

    return(          
      <div className={Styles.App}>
      <Cockpit 
        showlist={this.state.showlist}
        persons={this.state.persons}
        toggleList={this.toggleList}
      /> 
        {persons}
      </div>          
    )
  }
}
export default App;